package com.appbusquedapokemon.utilities;

import java.util.HashMap;

public class Const {
    public static final String NOT_FOUNDED_404 = "Sin resultados";
    public static final String INTERNAL_SERVER_500 = "Sucedió un error";
    public static final String CONNECTION_ERROR = "Error de conexión";

    public static final int INT_NOT_FOUNDED_404 = 404;
    public static final int INT_INTERNAL_SERVER_500 = 500;
    public static final int INT_CONNECTION_ERROR = -1;
    public static String getMessageError(int opt){
        return switch (opt) {
            case INT_NOT_FOUNDED_404 -> NOT_FOUNDED_404;
            case INT_INTERNAL_SERVER_500 -> INTERNAL_SERVER_500;
            default -> CONNECTION_ERROR;
        };
    }
}
