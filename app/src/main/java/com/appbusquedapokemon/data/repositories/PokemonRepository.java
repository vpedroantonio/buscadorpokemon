package com.appbusquedapokemon.data.repositories;

import static com.appbusquedapokemon.utilities.Const.INT_CONNECTION_ERROR;
import static com.appbusquedapokemon.utilities.Const.INT_INTERNAL_SERVER_500;
import static com.appbusquedapokemon.utilities.Const.INT_NOT_FOUNDED_404;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.appbusquedapokemon.data.models.PokemonModel;
import com.appbusquedapokemon.network.interfaces.IPokemonApi;

import java.util.Objects;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PokemonRepository {
    private static final String TAG = PokemonRepository.class.getSimpleName();
    private final IPokemonApi iPokemonApi;
    public MutableLiveData<PokemonModel> mPokemonModel = new MutableLiveData<>(null);
    public MutableLiveData<Integer> mApiErrors = new MutableLiveData<>(null);
    @Inject
    public PokemonRepository(IPokemonApi iPokemonApi) {
        this.iPokemonApi = iPokemonApi;
    }

    public void fetchPokemonByName(String name){
        iPokemonApi.searchPokemonByName(name).enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<PokemonModel> call, @NonNull Response<PokemonModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    mPokemonModel.setValue(response.body());
                    mPokemonModel.setValue(null);
                } else {
                    mApiErrors.setValue(response.code() >= 400 && response.code() < 500 ? INT_NOT_FOUNDED_404 : INT_INTERNAL_SERVER_500);
                    mApiErrors.setValue(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PokemonModel> call, @NonNull Throwable t) {
                Log.e(TAG, Objects.requireNonNull(t.getMessage()));
                mApiErrors.setValue(INT_CONNECTION_ERROR);
                mApiErrors.setValue(null);
            }
        });
    }

}
