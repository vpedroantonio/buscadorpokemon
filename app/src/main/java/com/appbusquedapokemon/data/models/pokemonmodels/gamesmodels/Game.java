package com.appbusquedapokemon.data.models.pokemonmodels.gamesmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Game implements Serializable {
    @SerializedName("game_index")
    private int gameIndex  = 0;
    @SerializedName("version")
    private Version version = new Version();

    public int getGameIndex() {
        return gameIndex;
    }

    public void setGameIndex(int gameIndex) {
        this.gameIndex = gameIndex;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameIndex=" + gameIndex +
                ", version=" + version +
                '}';
    }
}
