package com.appbusquedapokemon.data.models.pokemonmodels.spritesmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OfficialArtwork implements Serializable {
    @SerializedName("front_default")
    private String frontDefault = "";

    @SerializedName("front_shiny")
    private String frontShiny = "";

    public String getFrontDefault() {
        return frontDefault;
    }

    public String getFrontShiny() {
        return frontShiny;
    }

    public void setFrontDefault(String frontDefault) {
        this.frontDefault = frontDefault;
    }

    public void setFrontShiny(String frontShiny) {
        this.frontShiny = frontShiny;
    }

    @Override
    public String toString() {
        return "OfficialArtwork{" +
                "frontDefault='" + frontDefault + '\'' +
                ", frontShiny='" + frontShiny + '\'' +
                '}';
    }
}
