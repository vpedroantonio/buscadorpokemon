package com.appbusquedapokemon.data.models.pokemonmodels;

import com.appbusquedapokemon.data.models.pokemonmodels.spritesmodels.Other;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sprites implements Serializable {
    @SerializedName("other")
    private Other other = new Other();

    public Other getOther() {
        return other;
    }

    public void setOther(Other other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "Sprites{" +
                "other=" + other +
                '}';
    }
}
