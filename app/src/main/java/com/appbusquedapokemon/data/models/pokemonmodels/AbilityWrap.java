package com.appbusquedapokemon.data.models.pokemonmodels;

import com.appbusquedapokemon.data.models.pokemonmodels.abilitiesmodels.Ability;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AbilityWrap implements Serializable {
    @SerializedName("ability")
    private Ability ability = new Ability();

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }

    @Override
    public String toString() {
        return "AbilityWrap{" +
                "ability=" + ability +
                '}';
    }
}
