package com.appbusquedapokemon.data.models.pokemonmodels.spritesmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Other implements Serializable {
    @SerializedName("official-artwork")
    private OfficialArtwork officialArtwork = new OfficialArtwork();

    @SerializedName("showdown")
    private Showdown showdown = new Showdown();

    public OfficialArtwork getOfficialArtwork() {
        return officialArtwork;
    }

    public void setOfficialArtwork(OfficialArtwork officialArtwork) {
        this.officialArtwork = officialArtwork;
    }

    public Showdown getShowdown() {
        return showdown;
    }

    public void setShowdown(Showdown showdown) {
        this.showdown = showdown;
    }

    @Override
    public String toString() {
        return "Other{" +
                "officialArtwork=" + officialArtwork +
                ", showdown=" + showdown +
                '}';
    }
}
