package com.appbusquedapokemon.data.models;

import com.appbusquedapokemon.data.models.pokemonmodels.AbilityWrap;
import com.appbusquedapokemon.data.models.pokemonmodels.Sprites;
import com.appbusquedapokemon.data.models.pokemonmodels.gamesmodels.Game;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PokemonModel implements Serializable {

    @SerializedName("abilities")
    private List<AbilityWrap> abilityWrapList = new ArrayList<>();

    @SerializedName("sprites")
    private Sprites sprites = new Sprites();

    @SerializedName("game_indices")
    private List<Game> gameList = new ArrayList<>();

    public Sprites getSprites() {
        return sprites;
    }

    public void setSprites(Sprites sprites) {
        this.sprites = sprites;
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
    }

    public List<AbilityWrap> getAbilityWrapList() {
        return abilityWrapList;
    }

    public void setAbilityWrapList(List<AbilityWrap> abilityWrapList) {
        this.abilityWrapList = abilityWrapList;
    }

    @Override
    public String toString() {
        return "PokemonModel{" +
                "abilityWrapList=" + abilityWrapList +
                ", sprites=" + sprites +
                ", gameList=" + gameList +
                '}';
    }
}
