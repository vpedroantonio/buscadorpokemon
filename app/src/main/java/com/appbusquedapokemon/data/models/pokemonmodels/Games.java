package com.appbusquedapokemon.data.models.pokemonmodels;

import com.appbusquedapokemon.data.models.pokemonmodels.gamesmodels.Game;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Games implements Serializable {
    @SerializedName("game_indices")
    private List<Game> gameList = new ArrayList<>();

    public List<Game> getGameList() {
        return gameList;
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
    }

    @Override
    public String toString() {
        return "Games{" +
                "gameList=" + gameList +
                '}';
    }
}
