package com.appbusquedapokemon.data.models.pokemonmodels.abilitiesmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ability implements Serializable {
    @SerializedName("name")
    private String name = "";
    @SerializedName("url")
    private String url = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Ability{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
