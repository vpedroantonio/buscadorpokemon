package com.appbusquedapokemon.data.models.pokemonmodels.gamesmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Version implements Serializable {
    @SerializedName("name")
    private String name = "";

    public String getName() {
        return name.trim();
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Version{" +
                "name='" + name + '\'' +
                '}';
    }
}
