package com.appbusquedapokemon.data.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.appbusquedapokemon.R;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private List<String> valoresList = new ArrayList<>();

    public ListAdapter(List<String> valoresList) {
        this.valoresList = valoresList;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ListViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        holder.render(valoresList.get(position));
    }

    @Override
    public int getItemCount() {
        return valoresList.size();
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder{
        private final TextView tvValor;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvValor = itemView.findViewById(R.id.tvValor);
        }

        public void render(String v){
            tvValor.setText(v != null ? v : "");
        }
    }
}
