package com.appbusquedapokemon.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.appbusquedapokemon.data.models.PokemonModel;
import com.appbusquedapokemon.data.repositories.PokemonRepository;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class PokemonViewModel extends ViewModel {
    private final PokemonRepository pokemonRepository;
    @Inject
    public PokemonViewModel(PokemonRepository pokemonRepository) {
        this.pokemonRepository = pokemonRepository;
    }
    public void fetchPokemonByName(String name){
        pokemonRepository.fetchPokemonByName(name);
    }
    public MutableLiveData<PokemonModel> getmPokemonByName(){
        return pokemonRepository.mPokemonModel;
    }
    public MutableLiveData<Integer> getmErrors(){
        return pokemonRepository.mApiErrors;
    }
}

