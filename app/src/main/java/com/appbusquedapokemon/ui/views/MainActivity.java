package com.appbusquedapokemon.ui.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appbusquedapokemon.R;
import com.appbusquedapokemon.data.adapters.ListAdapter;
import com.appbusquedapokemon.data.models.PokemonModel;
import com.appbusquedapokemon.data.models.pokemonmodels.AbilityWrap;
import com.appbusquedapokemon.data.models.pokemonmodels.abilitiesmodels.Ability;
import com.appbusquedapokemon.data.models.pokemonmodels.gamesmodels.Game;
import com.appbusquedapokemon.data.models.pokemonmodels.gamesmodels.Version;
import com.appbusquedapokemon.databinding.ActivityMainBinding;
import com.appbusquedapokemon.ui.viewmodels.PokemonViewModel;
import com.appbusquedapokemon.utilities.Const;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity implements TextWatcher, View.OnFocusChangeListener, View.OnClickListener, TextView.OnEditorActionListener {
    private PokemonViewModel pokemonViewModel;
    private ActivityMainBinding activityBinding;
    private String pokemonCurrentSearch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityBinding.getRoot());

        addListeners();
        instViewModels();

    }
    private void addListeners(){
        activityBinding.txtSearch.addTextChangedListener(this);
        activityBinding.txtSearch.setOnFocusChangeListener(this);
        activityBinding.txtSearch.setOnEditorActionListener(this);
        activityBinding.imgBtnSearch.setOnClickListener(this);
        activityBinding.searchInputContainer.setOnClickListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        pokemonCurrentSearch = editable.toString().trim();
        activityBinding.viewInfoPokemon.setVisibility(View.GONE);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        activityBinding.txtSearch.setText("");
    }


    private void instViewModels(){
        pokemonViewModel = new ViewModelProvider(this).get(PokemonViewModel.class);
        pokemonViewModel.getmPokemonByName().observe(this, onPokemonEncontrado());
        pokemonViewModel.getmErrors().observe(this, onPokemonApiError());
    }

    private Observer<Integer> onPokemonApiError() {
        return codeError -> {
            if(codeError != null){
                confShimmer(false);
                Toast.makeText(this, Const.getMessageError(codeError), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void confShimmer(boolean activo){
        if(!activo){
            activityBinding.shimmer.hideShimmer();
            activityBinding.shimmer.setVisibility(View.GONE);
            return;
        }
        activityBinding.shimmer.startShimmer();
        activityBinding.shimmer.setVisibility(View.VISIBLE);
        activityBinding.viewInfoPokemon.setVisibility(View.GONE);
    }

    private Observer<PokemonModel> onPokemonEncontrado() {
        return pokemonInfo -> {
            if(pokemonInfo != null){
                minificarSearchLayout();
                bindValues(pokemonInfo);
            }
        };
    }

    private void bindValues(PokemonModel pokemonInfo) {
        try{
            Picasso.get().load(pokemonInfo.getSprites().getOther().getOfficialArtwork().getFrontDefault()).into(activityBinding.imgPokemon);
            activityBinding.tvNombrePokemon.setText(pokemonCurrentSearch);
            activityBinding.txtSearch.setText("");

            List<Game> gameVersionsList = pokemonInfo.getGameList();
            List<AbilityWrap> abilityWrapList = pokemonInfo.getAbilityWrapList();

            List<String> gameListFiltro = gameVersionsList.stream().map(Game::getVersion).map(Version::getName).collect(Collectors.toList());
            List<String> abilityList =  abilityWrapList.stream().map(AbilityWrap::getAbility).map(Ability::getName).collect(Collectors.toList());

            ListAdapter adapterGameVersions = new ListAdapter(gameListFiltro);
            ListAdapter adapterAbilities = new ListAdapter(abilityList);

            activityBinding.rvJuegos.setAdapter(adapterGameVersions);
            activityBinding.rvJuegos.setLayoutManager(new LinearLayoutManager(this));

            activityBinding.rvHabilidades.setAdapter(adapterAbilities);
            activityBinding.rvHabilidades.setLayoutManager(new LinearLayoutManager(this));

            confShimmer(false);
            activityBinding.viewInfoPokemon.setVisibility(View.VISIBLE);

        }catch (Exception e){
            Log.e(MainActivity.class.getSimpleName(), Objects.requireNonNull(e.getMessage()));

            confShimmer(false);
        }
    }
    private void minificarSearchLayout(){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(activityBinding.searchContainer.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        activityBinding.searchContainer.setLayoutParams(params);
        activityBinding.viewInfoPokemon.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.imgBtnSearch ){
            validarFetch();
        }
    }

    private void validarFetch(){
        if(pokemonCurrentSearch.length() >= 2){
            pokemonViewModel.fetchPokemonByName(pokemonCurrentSearch);
            confShimmer(true);
            return;
        }
        Toast.makeText(this, getString(R.string.busqueda_al_menos_2_caracteres), Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_NEXT ) {
            validarFetch();
            return true;
        }
        return false;
    }
}