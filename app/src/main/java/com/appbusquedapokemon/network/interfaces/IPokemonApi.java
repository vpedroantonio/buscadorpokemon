package com.appbusquedapokemon.network.interfaces;

import com.appbusquedapokemon.data.models.PokemonModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IPokemonApi {
    @GET("pokemon/{pokemon}")
    Call<PokemonModel> searchPokemonByName(
            @Path("pokemon") String pokemon
    );

}
